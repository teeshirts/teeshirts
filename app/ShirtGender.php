<?php namespace App;

class ShirtGender extends BaseModel {

    protected $table = 'tee_shirt_genders';

    protected $fillable = ['shirt_id', 'gender'];
}
