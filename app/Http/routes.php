<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Post shirt - step 1
Route::get('post/step1', [
    'uses' => 'WebsiteController@getPostStep1'
]);

//Post shirt - step 2
Route::get('post/step2', [
    'uses' => 'WebsiteController@getPostStep2'
]);

Route::controller('website', 'WebsiteController');
Route::controller('dashboard', 'DashboardController');


//TODO: id should be int only
Route::get('shirt/{id}', [
    'uses' => 'ShirtController@getDetails'
])->where(['id' => '[0-9]+']);

Route::controller('shirt', 'ShirtController');
Route::controller('shirts', 'ShirtsController');

Route::get('foo', 'FooController@foo');
