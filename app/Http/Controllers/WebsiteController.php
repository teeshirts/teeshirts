<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\FormFacade as Form;
use Illuminate\Support\Facades\DB;

class WebsiteController extends Controller {
    public $selectone = 'Select One';

    public function __construct(){

    }

    public function getIndex(){
        return view('website.home');
        //return $this->generateDropdownFromTable();
    }

    public function getPostStep1(){
        $shirttypedropdown = $this->generateShirtTypeDropdown();
        $occcategorydropdown = $this->generateOccasionalCategoryDropdown();
        $shirtfabricsdropdown = $this->generateFabricsDropdown();

        return view('website.step1')->with([
            'shirttypesdropdown' => $shirttypedropdown,
            'occcategorydropdown' => $occcategorydropdown,
            'shirtfabrics' => $shirtfabricsdropdown
        ]);
    }

    public function getPostStep2(){
        $locationdropdown = $this->generateLocationDropdown();
        return view('website.step2')->with([
            'locationdropdown' => $locationdropdown
        ]);
    }

    public function getPostStep3(){
        return 'Post shirt step 1';
    }

    public function getAbout(){
        return 'About us page';
    }

    //Bunch of helper  methods
    public function setUUIDToRequest(Request $request){
        $request->merge(['uuid' => uniqid()]);
    }

    public function getSelectOne(){
        return $this->selectone;
    }

    public function generateShirtTypeDropdown($name='shirttype', $selected='', $attr=['class' => 'form-control']){
        return $this->generateDropdownFromTable('tee_base_shirt_types', $name, $selected, $attr);
    }

    public function generateLocationDropdown($name='location', $selected='', $attr=['class' => 'form-control']){
        return $this->generateDropdownFromTable('tee_base_locations', $name, $selected, $attr);
    }

    public function generateOccasionalCategoryDropdown($name='shirtcategory', $selected='', $attr=['class' => 'form-control']){
        return $this->generateDropdownFromTable('tee_base_shirt_categories', $name, $selected, $attr);
    }

    public function generateFabricsDropdown($name='shirtfabrictype', $selected='', $attr=['class' => 'form-control']){
        return $this->generateDropdownFromTable('tee_base_shirt_fabrics', $name, $selected, $attr);
    }

    public function generateDropdownFromTable($tablename='tee_base_shirt_categories', $name='name', $selected='', $attr=['class' => 'form-control']){
        $tabledropdown = '';
        $tablearr = [];
        $tablearr[''] = $this->getSelectOne();

        $tabledata = DB::table($tablename)->get();
        if(!empty($tabledata)){
            foreach($tabledata as $data){
                $tablearr[$data->uuid] = $data->name;
            }
        }

        $tabledropdown = Form::select($name, $tablearr, $selected, $attr);
        return $tabledropdown;
    }

    public function getTest(){
        $sizes = ['x', 'xl'];
        return implode(',', $sizes);
    }


}