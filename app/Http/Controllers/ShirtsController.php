<?php namespace App\Http\Controllers;

use App\Shirt;

class ShirtsController extends Controller {

    public function getIndex(){
        $shirts = Shirt::where('status', 'published')->get();

        return view('website.shirts')->with('shirts', $shirts);
    }
}