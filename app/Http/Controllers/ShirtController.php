<?php namespace App\Http\Controllers;

use App\BaseLocation;
use App\ShirtGender;
use App\UserContact;
use Illuminate\Http\Request;
use App\Shirt;
use App\BaseShirtType;
use App\ShirtType;
use App\BaseShirtCategory;
use App\BaseShirtFabric;
use App\ShirtFabric;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\ShirtCategory;
use App\User;

class ShirtController extends Controller {

    public function getDetails($id){
        try{
            $shirt = Shirt::where('id', $id)->firstOrFail();
            //$shirt = Shirt::where('id', $id)->where('status', 'published')->firstOrFail();

            $shirt->shirt_type = BaseShirtType::find($shirt->type->shirt_type_id)->name;
            unset($shirt->type);

            $shirt->shirt_category = BaseShirtCategory::find($shirt->category->category_id)->name;
            unset($shirt->category);

            $shirt->shirt_fabric = BaseShirtFabric::find($shirt->fabric->shirt_fabric_id)->name;
            unset($shirt->fabric);

            //$shirt->genders;
            $shirtgenders = $shirt->genders;
            $shirtgendersarr= [];
            if(!empty($shirtgenders)){
                foreach($shirtgenders as $shirtgender){
                    $shirtgendersarr[] = $shirtgender->gender;
                }

                $shirt->shirt_genders = $shirtgendersarr;
            }
            unset($shirt->genders);

            return view('website.shirt')->with('shirt', $shirt);
            //return $shirt;
        }
        catch(ModelNotFoundException $e){
            return 'Shirt not found or access denied';
        }
    }

    public function postStep2(Request $request){
        //TODO: Check if email exists in db
        if($request->has('email_address')){
            $inputemailaddress = $request->input('email_address');
            $emailexists = false;

            try{
                $seller = User::where('email_address', $inputemailaddress)->firstOrFail();
                $emailexists = true;
            }
            catch(ModelNotFoundException $e){
                $emailexists = false;
            }

            if($emailexists){
                //TODO: Return to step 2 with errors
                return 'Email already exists in the db. Please choose a new one';
            }
            else{
                //TODO: Create new user
                $selleraccount = new User();
                $idlocation = 0;

                try{
                    $location = BaseLocation::where('uuid', $request->input('location'))->firstOrFail();
                    $idlocation = $location->id;
                }
                catch(ModelNotFoundException $e){
                    $idlocation = 0;
                }

                $selleraccount->brand_name = $request->input('brand_name');
                $selleraccount->email_address = $request->input('email_address');

                $selleraccount->location_id = $idlocation;

                //Save the new user account
                $selleraccount->save();

                $selleraccountid = $selleraccount->id;

                //Save user contact
                if($request->has('contactnumber')){
                    $selleraccountarr = [];
                    $inputcontactnumber = $request->input('contactnumber');

                    foreach($inputcontactnumber as $contactnumber){
                        $selleraccountarr[] = new UserContact([
                            'user_id' => $selleraccountid,
                            'contact_number' => $contactnumber,
                            'network' => '' //TODO: Need to get the network type based on the contact number
                        ]);
                    }

                    //return $selleraccountarr;
                    $selleraccount->contacts()->saveMany($selleraccountarr);
                }

            }
        }
    }

    public function postCreate(Request $request){

        $shirt = new Shirt();
        $shirt->name = $request->input('name');
        $shirt->regular_price = $request->input('regular_price');
        $shirt->uuid = uniqid();
        $shirt->status = 'draft';

        //Save sizes
        if($request->has('size')){
            $inputshirtsizes = $request->input('size');
            $shirtsizes = implode(',', $inputshirtsizes);
            $shirt->sizes = $shirtsizes;
        }

        //Save colors
        if($request->has('color')){
            $inputshirtcolors = $request->input('color');
            $shirtcolors = implode(',', $inputshirtcolors);
            $shirt->colors = $shirtcolors;
        }

        if(! $shirt->save()){
            //TODO: Show errors here and redirect with errors
            return 'Error occured on saving shirt';
        }
        else{
            $shirtuuid = $shirt->uuid;
            $this->setActiveShirt($shirtuuid);

            $activeshirt = Shirt::where('uuid', $shirtuuid)->firstOrFail();
            $activeshirtid = $activeshirt->id;

            //Save shirt type
            if($request->has('shirttype')){
                $shirtypeid = 0;
                try{
                    $shirttype = BaseShirtType::where('uuid', $request->input('shirttype'))->firstOrFail();
                    $shirttypeid = $shirttype->id;
                }
                catch(ModelNotFoundException $e){

                }

                $shirttypearr = new ShirtType(['shirt_id' => $activeshirtid, 'shirt_type_id' => $shirttypeid]);
                $activeshirt->type()->save($shirttypearr);
            }

            //Save category
            if($request->has('shirtcategory')){
                $shirtcategoryid = 0;
                try{
                    $shirtcategory = BaseShirtCategory::where('uuid', $request->input('shirtcategory'))->firstOrFail();
                    $shirtcategoryid = $shirtcategory->id;
                }
                catch(ModelNotFoundException $e){

                }

                $shirtcategoryarr = new ShirtCategory(['shirt_id' => $activeshirtid, 'category_id' => $shirtcategoryid]);
                $activeshirt->category()->save($shirtcategoryarr);
            }

            //Save Fabrics
            if($request->has('shirtfabrictype')){
                $shirtfabricid = 0;
                try{
                    $shirtfabric = BaseShirtFabric::where('uuid', $request->input('shirtfabrictype'))->firstOrFail();
                    $shirtfabricid = $shirtfabric->id;
                }
                catch(ModelNotFoundException $e){

                }

                $shirtfabricarr = new ShirtFabric(['shirt_id' => $activeshirtid, 'shirt_fabric_id' => $shirtfabricid]);
                $activeshirt->fabric()->save($shirtfabricarr);
            }

            //Save gender
            if($request->has('gender')){
                //TODO: In JS, if both is checked, checked both male and female
                $shirtgenderarr = [];
                $inputshirtgender = $request->input('gender');
                foreach($inputshirtgender as $shirtgender){
                    $shirtgenderarr[] = new ShirtGender([
                        'shirt_id' => $activeshirtid,
                        'gender' => $shirtgender
                    ]);
                }

                $activeshirt->genders()->saveMany($shirtgenderarr);
            }

            //Save shirt image
        }
    }

    public function setActiveShirt($shirtuuid=0){
        Session::put('shirtuuid', $shirtuuid);
    }

    public function getActiveShirt(){
        return Session::get('shirtuuid');
    }

    public function getTest(){

    }
}