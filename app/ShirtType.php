<?php namespace App;

class ShirtType extends BaseModel {

    protected $table = 'tee_shirt_types';

    protected $fillable = ['shirt_id', 'shirt_type_id'];
}