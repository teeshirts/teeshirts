<?php namespace App;

class UserContact extends BaseModel {

    protected $table = 'tee_account_contact';

    protected $fillable = ['user_id', 'contact_number', 'network'];
}