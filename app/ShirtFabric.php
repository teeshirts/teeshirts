<?php namespace App;

class ShirtFabric extends BaseModel {

    protected $table = 'tee_shirt_fabrics';

    protected $fillable = ['shirt_id', 'shirt_fabric_id'];
}