<?php namespace App;

class BaseLocation extends BaseModel {

    protected $table = 'tee_base_locations';

    protected $fillable = ['uuid', 'name'];
}