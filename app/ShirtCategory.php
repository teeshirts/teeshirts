<?php namespace App;

class ShirtCategory extends BaseModel {

    protected $table = 'tee_shirt_categories';

    protected $fillable = ['shirt_id', 'category_id'];
}