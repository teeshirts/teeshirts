<?php namespace App;

use Watson\Validating\ValidatingTrait;
class Shirt extends BaseModel {
    use ValidatingTrait;

    protected $table = 'tee_shirts';

    protected $fillable = ['uuid', 'user_id', 'name', 'description', 'status', 'is_featured', 'fb_post_id', 'regular_price', 'sizes', 'colors'];

    protected $rules = [
        'uuid' => 'required'
    ];

    public function type(){
        return $this->hasOne('App\ShirtType', 'shirt_id', 'id');
    }

    public function category(){
        return $this->hasOne('App\ShirtCategory', 'shirt_id', 'id');
    }

    public function fabric(){
        return $this->hasOne('App\ShirtFabric', 'shirt_id', 'id');
    }

    public function genders(){
        return $this->hasMany('App\ShirtGender', 'shirt_id', 'id');
    }


}