-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for teeshirts
DROP DATABASE IF EXISTS `teeshirts`;
CREATE DATABASE IF NOT EXISTS `teeshirts` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `teeshirts`;


-- Dumping structure for table teeshirts.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table teeshirts.migrations: ~12 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2015_01_26_125958_tee_token', 1),
	('2015_01_27_080029_tee_account', 1),
	('2015_02_01_102001_create_tee_shirts_table', 1),
	('2015_02_01_102025_create_tee_shirt_color_entities_table', 1),
	('2015_02_01_102043_create_tee_shirt_fabric_entities_table', 1),
	('2015_02_01_102105_create_tee_shirt_genders_table', 1),
	('2015_02_10_232037_create_tee_shirt_categories_table', 1),
	('2015_02_14_015619_update_tee_shirt_fabric_entities_table', 1),
	('2015_02_20_094014_add_description_to_tee_shirts_table', 1),
	('2015_02_20_101932_create_tee_shirt_images_table', 1),
	('2015_03_15_070851_create_tee_account_contact_table', 2),
	('2015_03_22_065432_create_tee_shirt_size_entities_table', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_accounts
DROP TABLE IF EXISTS `tee_accounts`;
CREATE TABLE IF NOT EXISTS `tee_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_photo` text COLLATE utf8_unicode_ci NOT NULL,
  `cover_photo` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table teeshirts.tee_accounts: ~4 rows (approximately)
/*!40000 ALTER TABLE `tee_accounts` DISABLE KEYS */;
INSERT INTO `tee_accounts` (`id`, `brand_name`, `email_address`, `location_id`, `password`, `user_type`, `profile_photo`, `cover_photo`, `description`, `gender`, `created_at`, `updated_at`) VALUES
	(1, 'test brand', 'test@company.com', 1, '', '', '', '', '', '', '2015-06-22 13:43:48', '2015-06-22 13:43:48'),
	(2, 'tes', 'test@company1.com', 1, '', '', '', '', '', '', '2015-06-22 13:45:14', '2015-06-22 13:45:14'),
	(3, 'test brand', 'test@company2.com', 2, '', '', '', '', '', '', '2015-06-22 13:45:52', '2015-06-22 13:45:52'),
	(4, 'test brand', 'test@company3.com', 2, '', '', '', '', '', '', '2015-06-22 13:49:05', '2015-06-22 13:49:05');
/*!40000 ALTER TABLE `tee_accounts` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_account_contact
DROP TABLE IF EXISTS `tee_account_contact`;
CREATE TABLE IF NOT EXISTS `tee_account_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `contact_number` varchar(11) NOT NULL,
  `network` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table teeshirts.tee_account_contact: ~6 rows (approximately)
/*!40000 ALTER TABLE `tee_account_contact` DISABLE KEYS */;
INSERT INTO `tee_account_contact` (`id`, `user_id`, `contact_number`, `network`, `created_at`, `updated_at`) VALUES
	(1, 1, '92345678', '', '2015-06-22 13:43:48', '2015-06-22 13:43:48'),
	(2, 1, '93456789', '', '2015-06-22 13:43:48', '2015-06-22 13:43:48'),
	(3, 3, '92345678', '', '2015-06-22 13:45:52', '2015-06-22 13:45:52'),
	(4, 3, '93456789', '', '2015-06-22 13:45:52', '2015-06-22 13:45:52'),
	(5, 4, '092345678', '', '2015-06-22 13:49:05', '2015-06-22 13:49:05'),
	(6, 4, '093456789', '', '2015-06-22 13:49:05', '2015-06-22 13:49:05');
/*!40000 ALTER TABLE `tee_account_contact` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_base_locations
DROP TABLE IF EXISTS `tee_base_locations`;
CREATE TABLE IF NOT EXISTS `tee_base_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table teeshirts.tee_base_locations: ~2 rows (approximately)
/*!40000 ALTER TABLE `tee_base_locations` DISABLE KEYS */;
INSERT INTO `tee_base_locations` (`id`, `uuid`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'asdfafdf', 'Cebu', '2015-06-22 21:41:04', '2015-06-22 21:41:07'),
	(2, 'asdfdfd', 'Bacolod', '2015-06-22 21:41:21', '2015-06-22 21:41:22');
/*!40000 ALTER TABLE `tee_base_locations` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_base_shirt_categories
DROP TABLE IF EXISTS `tee_base_shirt_categories`;
CREATE TABLE IF NOT EXISTS `tee_base_shirt_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table teeshirts.tee_base_shirt_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `tee_base_shirt_categories` DISABLE KEYS */;
INSERT INTO `tee_base_shirt_categories` (`id`, `uuid`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'asdfadfaf', 'Shirt Category 1', '2015-06-12 13:48:13', '2015-06-12 13:48:13'),
	(2, 'asdfadfaf2', 'Shirt Category 2', '2015-06-12 13:48:13', '2015-06-12 13:48:13'),
	(3, 'asdfadfaf3', 'Shirt Category 3', '2015-06-12 13:48:13', '2015-06-12 13:48:13');
/*!40000 ALTER TABLE `tee_base_shirt_categories` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_base_shirt_fabrics
DROP TABLE IF EXISTS `tee_base_shirt_fabrics`;
CREATE TABLE IF NOT EXISTS `tee_base_shirt_fabrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table teeshirts.tee_base_shirt_fabrics: ~2 rows (approximately)
/*!40000 ALTER TABLE `tee_base_shirt_fabrics` DISABLE KEYS */;
INSERT INTO `tee_base_shirt_fabrics` (`id`, `uuid`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'adfdfd', 'Shirt Fabric 1', '2015-06-12 13:51:23', '2015-06-12 13:51:23'),
	(2, 'adfdfd 2', 'Shirt Fabric 2', '2015-06-12 13:51:23', '2015-06-12 13:51:23'),
	(3, 'adfdfd 3', 'Shirt Fabric 3', '2015-06-12 13:51:23', '2015-06-12 13:51:23');
/*!40000 ALTER TABLE `tee_base_shirt_fabrics` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_base_shirt_types
DROP TABLE IF EXISTS `tee_base_shirt_types`;
CREATE TABLE IF NOT EXISTS `tee_base_shirt_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table teeshirts.tee_base_shirt_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `tee_base_shirt_types` DISABLE KEYS */;
INSERT INTO `tee_base_shirt_types` (`id`, `uuid`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'asdfaf', 'Shirt Type 1', '2015-06-12 13:50:10', '2015-06-12 13:50:10'),
	(2, 'asdfaf2', 'Shirt Type 2', '2015-06-12 13:50:10', '2015-06-12 13:50:10'),
	(3, '3', 'Shirt Type 3', '2015-06-12 13:50:10', '2015-06-12 13:50:10');
/*!40000 ALTER TABLE `tee_base_shirt_types` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_shirts
DROP TABLE IF EXISTS `tee_shirts`;
CREATE TABLE IF NOT EXISTS `tee_shirts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_featured` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'N',
  `fb_post_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sizes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colors` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regular_price` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table teeshirts.tee_shirts: ~19 rows (approximately)
/*!40000 ALTER TABLE `tee_shirts` DISABLE KEYS */;
INSERT INTO `tee_shirts` (`id`, `uuid`, `user_id`, `name`, `description`, `status`, `is_featured`, `fb_post_id`, `sizes`, `colors`, `regular_price`, `created_at`, `updated_at`) VALUES
	(1, '', 1, 'Dummy shirt by Alfie 1', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-03-22 08:41:28', '2015-03-22 08:41:28'),
	(2, '', 2, 'Dummy shirt by Alfie 1', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-03-22 08:48:46', '2015-03-22 08:48:46'),
	(3, '', 3, 'Dummy shirt by Alfie 1', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-03-22 08:51:52', '2015-03-22 08:51:52'),
	(4, '', 4, 'Dummy shirt by Alfie 1', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-03-22 08:59:30', '2015-03-22 08:59:30'),
	(5, '', 0, '0', '', 'N', 'N', NULL, NULL, NULL, 0, '2015-06-13 01:33:44', '2015-06-13 01:33:44'),
	(6, '', 0, '0', '', 'N', 'N', NULL, NULL, NULL, 0, '2015-06-13 01:34:41', '2015-06-13 01:34:41'),
	(7, '557b917922646', 0, 'test', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-06-13 02:12:09', '2015-06-13 02:12:09'),
	(8, '557b91c1dd1a0', 0, 'test name', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-06-13 02:13:21', '2015-06-13 02:13:21'),
	(9, '557b927888339', 0, 'test name', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-06-13 02:16:24', '2015-06-13 02:16:24'),
	(10, '557b94aad62ef', 0, 'test 001', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-06-13 02:25:46', '2015-06-13 02:25:46'),
	(11, '557b94f4c2b39', 0, 'test 001', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-06-13 02:27:00', '2015-06-13 02:27:00'),
	(12, '557b95065bfa7', 0, 'test 002', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-06-13 02:27:18', '2015-06-13 02:27:18'),
	(13, '557b986e8758e', 0, 'test 003', '', 'N', 'N', NULL, NULL, NULL, 13, '2015-06-13 02:41:50', '2015-06-13 02:41:50'),
	(14, '557b9b91c901e', 0, 'test 004', '', 'N', 'N', NULL, NULL, NULL, 12, '2015-06-13 02:55:14', '2015-06-13 02:55:14'),
	(15, '557b9bbc8669b', 0, 'test 005', '', 'draft', 'N', NULL, NULL, NULL, 14, '2015-06-13 02:55:56', '2015-06-13 02:55:56'),
	(16, '557b9be8801f1', 0, 'test 006', '', 'published', 'N', NULL, NULL, NULL, 16, '2015-06-13 02:56:40', '2015-06-13 02:56:40'),
	(17, '557bd25858c59', 0, '', '', 'draft', 'N', NULL, NULL, NULL, 0, '2015-06-13 06:48:56', '2015-06-13 06:48:56'),
	(18, '557bd26a5c7c5', 0, '', '', 'draft', 'N', NULL, NULL, NULL, 0, '2015-06-13 06:49:14', '2015-06-13 06:49:14'),
	(19, '557bd298a4b73', 0, '', '', 'draft', 'N', NULL, 'xs', NULL, 0, '2015-06-13 06:50:00', '2015-06-13 06:50:00'),
	(20, '557bd2a9a3beb', 0, '', '', 'draft', 'N', NULL, NULL, NULL, 0, '2015-06-13 06:50:17', '2015-06-13 06:50:17'),
	(21, '557bd4ea46bcf', 0, 'test shirt 01', '', 'draft', 'N', NULL, 'xs,s', 'white,black', 21, '2015-06-13 06:59:54', '2015-06-13 06:59:54'),
	(22, '557bd58046902', 0, 'test shirt 02', '', 'draft', 'N', NULL, 'xs,s', 'white,black', 23, '2015-06-13 07:02:24', '2015-06-13 07:02:24');
/*!40000 ALTER TABLE `tee_shirts` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_shirt_categories
DROP TABLE IF EXISTS `tee_shirt_categories`;
CREATE TABLE IF NOT EXISTS `tee_shirt_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shirt_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table teeshirts.tee_shirt_categories: ~11 rows (approximately)
/*!40000 ALTER TABLE `tee_shirt_categories` DISABLE KEYS */;
INSERT INTO `tee_shirt_categories` (`id`, `shirt_id`, `category_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2015-03-22 08:41:28', '2015-03-22 08:41:28'),
	(2, 1, 2, '2015-03-22 08:41:28', '2015-03-22 08:41:28'),
	(3, 2, 1, '2015-03-22 08:48:46', '2015-03-22 08:48:46'),
	(4, 2, 2, '2015-03-22 08:48:46', '2015-03-22 08:48:46'),
	(5, 3, 1, '2015-03-22 08:51:52', '2015-03-22 08:51:52'),
	(6, 3, 2, '2015-03-22 08:51:52', '2015-03-22 08:51:52'),
	(7, 4, 1, '2015-03-22 08:59:31', '2015-03-22 08:59:31'),
	(8, 4, 2, '2015-03-22 08:59:31', '2015-03-22 08:59:31'),
	(9, 11, 1, '2015-06-13 02:27:00', '2015-06-13 02:27:00'),
	(10, 12, 1, '2015-06-13 02:27:18', '2015-06-13 02:27:18'),
	(11, 13, 1, '2015-06-13 02:41:50', '2015-06-13 02:41:50'),
	(12, 16, 1, '2015-06-13 02:56:40', '2015-06-13 02:56:40'),
	(13, 21, 2, '2015-06-13 06:59:54', '2015-06-13 06:59:54'),
	(14, 22, 2, '2015-06-13 07:02:24', '2015-06-13 07:02:24');
/*!40000 ALTER TABLE `tee_shirt_categories` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_shirt_fabrics
DROP TABLE IF EXISTS `tee_shirt_fabrics`;
CREATE TABLE IF NOT EXISTS `tee_shirt_fabrics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shirt_id` int(11) NOT NULL DEFAULT '0',
  `shirt_fabric_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table teeshirts.tee_shirt_fabrics: ~8 rows (approximately)
/*!40000 ALTER TABLE `tee_shirt_fabrics` DISABLE KEYS */;
INSERT INTO `tee_shirt_fabrics` (`id`, `shirt_id`, `shirt_fabric_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 0, '2015-03-22 08:41:28', '2015-03-22 08:41:28'),
	(2, 1, 0, '2015-03-22 08:41:28', '2015-03-22 08:41:28'),
	(3, 2, 0, '2015-03-22 08:48:46', '2015-03-22 08:48:46'),
	(4, 2, 0, '2015-03-22 08:48:46', '2015-03-22 08:48:46'),
	(5, 3, 0, '2015-03-22 08:51:52', '2015-03-22 08:51:52'),
	(6, 3, 0, '2015-03-22 08:51:52', '2015-03-22 08:51:52'),
	(7, 4, 0, '2015-03-22 08:59:30', '2015-03-22 08:59:30'),
	(8, 4, 0, '2015-03-22 08:59:31', '2015-03-22 08:59:31'),
	(9, 12, 1, '2015-06-13 02:41:50', '2015-06-13 02:41:50'),
	(10, 16, 1, '2015-06-13 02:56:40', '2015-06-13 02:56:40'),
	(11, 21, 3, '2015-06-13 06:59:54', '2015-06-13 06:59:54'),
	(12, 22, 2, '2015-06-13 07:02:24', '2015-06-13 07:02:24');
/*!40000 ALTER TABLE `tee_shirt_fabrics` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_shirt_genders
DROP TABLE IF EXISTS `tee_shirt_genders`;
CREATE TABLE IF NOT EXISTS `tee_shirt_genders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shirt_id` int(11) NOT NULL DEFAULT '0',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table teeshirts.tee_shirt_genders: ~2 rows (approximately)
/*!40000 ALTER TABLE `tee_shirt_genders` DISABLE KEYS */;
INSERT INTO `tee_shirt_genders` (`id`, `shirt_id`, `gender`, `created_at`, `updated_at`) VALUES
	(1, 22, 'm', '2015-06-13 07:02:24', '2015-06-13 07:02:24'),
	(2, 22, 'f', '2015-06-13 07:02:24', '2015-06-13 07:02:24');
/*!40000 ALTER TABLE `tee_shirt_genders` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_shirt_images
DROP TABLE IF EXISTS `tee_shirt_images`;
CREATE TABLE IF NOT EXISTS `tee_shirt_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shirt_id` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table teeshirts.tee_shirt_images: ~0 rows (approximately)
/*!40000 ALTER TABLE `tee_shirt_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `tee_shirt_images` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_shirt_types
DROP TABLE IF EXISTS `tee_shirt_types`;
CREATE TABLE IF NOT EXISTS `tee_shirt_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shirt_id` int(11) NOT NULL DEFAULT '0',
  `shirt_type_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table teeshirts.tee_shirt_types: ~8 rows (approximately)
/*!40000 ALTER TABLE `tee_shirt_types` DISABLE KEYS */;
INSERT INTO `tee_shirt_types` (`id`, `shirt_id`, `shirt_type_id`, `created_at`, `updated_at`) VALUES
	(9, 9, 1, '2015-06-13 02:16:24', '2015-06-13 02:16:24'),
	(10, 10, 1, '2015-06-13 02:25:46', '2015-06-13 02:25:46'),
	(11, 11, 1, '2015-06-13 02:27:00', '2015-06-13 02:27:00'),
	(12, 12, 1, '2015-06-13 02:27:18', '2015-06-13 02:27:18'),
	(13, 13, 1, '2015-06-13 02:41:50', '2015-06-13 02:41:50'),
	(14, 14, 1, '2015-06-13 02:55:14', '2015-06-13 02:55:14'),
	(15, 15, 1, '2015-06-13 02:55:56', '2015-06-13 02:55:56'),
	(16, 16, 1, '2015-06-13 02:56:40', '2015-06-13 02:56:40'),
	(17, 21, 1, '2015-06-13 06:59:54', '2015-06-13 06:59:54'),
	(18, 22, 2, '2015-06-13 07:02:24', '2015-06-13 07:02:24');
/*!40000 ALTER TABLE `tee_shirt_types` ENABLE KEYS */;


-- Dumping structure for table teeshirts.tee_token
DROP TABLE IF EXISTS `tee_token`;
CREATE TABLE IF NOT EXISTS `tee_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `expiration` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table teeshirts.tee_token: ~0 rows (approximately)
/*!40000 ALTER TABLE `tee_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `tee_token` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
