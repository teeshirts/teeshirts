@extends('layouts.website')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>Shirt name: {{ $shirt->name }}</p>
                <p>Price: {{ $shirt->regular_price }}</p>
                <p>Type: {{ $shirt->shirt_type }}</p>
                <p>Category: {{ $shirt->shirt_category }}</p>
                <p>Fabric: {{ $shirt->shirt_fabric }}</p>
                <p>Sizes: {{ $shirt->sizes}}</p>
                <p>Colors: {{ $shirt->colors}}</p>
                <p>Genders: {{ implode(',', $shirt->shirt_genders) }}</p>

            </div>
        </div>
    </div>
@stop