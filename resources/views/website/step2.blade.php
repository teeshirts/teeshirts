@extends('layouts.website')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => 'shirt/step2']) !!}
                    <div class="form-group">
                        <label for="">Brand</label>
                        <input class="form-control" type="text" placeholder="" name="brand_name"/>
                    </div>
                    <div class="form-group">
                        <label for="">Email address</label>
                        <input class="form-control" type="text" placeholder="" name="email_address"/>
                    </div>
                    <div class="form-group">
                        <label for="">Location</label>
                        {!! $locationdropdown !!}
                    </div>
                    <div class="form-group">
                        <label for="">Contact Number <small>Network type will be check on the backend</small></label>
                        <input class="form-control" type="text" name="contactnumber[]" value="092345678"/>
                        <input class="form-control" type="text" name="contactnumber[]" value="093456789"/>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Preview"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop