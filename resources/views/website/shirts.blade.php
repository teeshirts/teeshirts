@extends('layouts.website')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(!empty($shirts))
                    @foreach($shirts as $shirt)
                        <p>Name: {{ $shirt->name }}</p>
                        <a href="{{ URL::to('shirt/' . $shirt->id) }}">Click</a>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@stop