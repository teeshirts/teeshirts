@extends('layouts.website')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => 'shirt/create']) !!}
                    <div class="form-group">
                        <label for="">Shirt Image</label>
                        <input class="form-control" type="file"/>
                    </div>
                    <div class="form-group">
                        <label for="">Shirt Name</label>
                        <input class="form-control" type="text" name="name"/>
                    </div>
                    <div class="form-group">
                        <label for="">Type</label>
                        {!! $shirttypesdropdown !!}
                    </div>
                    <div class="form-group">
                        <label for="">Occasional Category</label>
                        {!! $occcategorydropdown !!}
                    </div>
                    <div class="form-group">
                        <label for="">Fabrics Used</label>
                        {!! $shirtfabrics !!}
                    </div>
                    <div class="form-group">
                        <label for="">Sizes Available</label>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="size[]" value="xs"/> XS
                        </div>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="size[]" value="s"/> S
                        </div>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="size[]" value="m"/> M
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="">Gender Available</label>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="gender[]" value="m"/> Male
                        </div>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="gender[]" value="f"/> Female
                        </div>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="gender[]" value="both"/> Both
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="">Colors Available</label>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="color[]" value="white"/> White
                        </div>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="color[]" value="black"/> Black
                        </div>
                        <div class="checkbox-inline">
                            <input type="checkbox" name="color[]" value="yellow"/> Yellow
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="">Price</label>
                        <input class="form-control" type="text" name="regular_price"/>
                    </div>
                    <input type="submit" value="Proceed"/>
                </form>
            </div>
        </div>
    </div>
@stop
